require  File.expand_path(File.join(File.dirname(__FILE__), '..', 'lib', 'prime_generator'))
  

describe PrimeGenerator do 


  it 'should generate the first prime and then a sequence of primes' do
    pg = PrimeGenerator.new
    pg.next.should == 2
    pg.next.should == 3
    pg.next.should == 5
    pg.next.should == 7
  end


  it 'should generate the 5th, 6th  and 7th  primes' do
    pg = PrimeGenerator.new(5)
    pg.next.should == 11
    pg.next.should == 13
    pg.next.should == 17
  end


end
