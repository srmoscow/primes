require_relative '../lib/prime_table'

describe  'primes' do

  let(:primes_executable)  { File.expand_path(File.join(File.dirname(__FILE__), '..', 'bin', 'primes')) }

  it 'should print the version' do
    %x{#{primes_executable} -v}.should == "primes 0.0.1 (c) 2014 Stephen Richards\n"
  end


  it 'should print usage' do 
    %x{#{primes_executable} -h}.should =~ /Usage:/
  end


  it 'should print a table of the first 10 primes if no parameters are given' do
    expected = "       2    3    5    7   11   13   17   19   23   29\n" +
               "  2    4    6   10   14   22   26   34   38   46   58\n" +
               "  3    6    9   15   21   33   39   51   57   69   87\n" +
               "  5   10   15   25   35   55   65   85   95  115  145\n" +
               "  7   14   21   35   49   77   91  119  133  161  203\n" +
               " 11   22   33   55   77  121  143  187  209  253  319\n" +
               " 13   26   39   65   91  143  169  221  247  299  377\n" +
               " 17   34   51   85  119  187  221  289  323  391  493\n" +
               " 19   38   57   95  133  209  247  323  361  437  551\n" +
               " 23   46   69  115  161  253  299  391  437  529  667\n" +
               " 29   58   87  145  203  319  377  493  551  667  841\n"
    %x/#{primes_executable}/.should == expected
  end  


  it 'should print a table of the 10th, 11th and 12th prinmes' do
    expected =  "        29    31    37\n" +
                "  29   841   899  1073\n" +
                "  31   899   961  1147\n" +
                "  37  1073  1147  1369\n"
    %x/#{primes_executable} -s 10 -c 3/.should == expected
  end
                 
end