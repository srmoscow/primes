

# This class will generate primes.  Once instantiated, calling next() will return the next prime in sequence.


class PrimeGenerator

  # instantiates a PrimeGen object. 
  #
  # @param [Fixnum] number_of_primes_to_generate: seed the PrimeGenerator to be ready to produce the nth prime at the first call to next().
  def initialize(number_of_primes_to_generate = 1)
    @last_prime = 0
    (number_of_primes_to_generate - 1).times do 
      generate_next 
    end
  end


  # return the next prime
  def next
    generate_next
    @last_prime
  end


  private

  def generate_next
    if @last_prime == 0
      @last_prime = 2
    else
      next_candidate = @last_prime + 1
      until is_prime?(next_candidate) do 
        next_candidate += 1
      end
      @last_prime = next_candidate
    end
  end


  def is_prime?(num)
    is_prime = true
    (2..num - 1).each do |i|
      if num % i == 0
        is_prime = false
        break
      end
    end
    return is_prime
  end


end

