require_relative 'prime_generator'

class PrimeTable

  def initialize(start, count)
    @start = start
    @count = count
    @max_num_length = nil
  end


  def run
    lines = generate_table
    lines.each { |line| puts line }
  end

  private


  def generate_table
    lines = []
    primes = get_primes
    calculate_max_num_length(primes)
    lines << generate_header_line(primes)
    primes.each { |p| lines << generate_body_lines(p, primes) }
    lines
  end


  # gets the array of primes wer are going to print from the prime generator
  def get_primes
    pg = PrimeGenerator.new(@start)
    primes = []
    @count.times { primes << pg.next }
    return primes
  end


  # calculates the width of the biggest number we're going to print
  def calculate_max_num_length(primes)
    @max_num_length = (primes.last * primes.last).to_s.size
  end



  def generate_header_line(primes)
    header_line = sprintf("%#{@max_num_length}s", "")
    primes.each do |p|
      header_line << sprintf("  %#{@max_num_length}d", p)
    end
    header_line
  end


  def generate_body_lines(row, primes)
    line = sprintf("%#{@max_num_length}d", row)
    primes.each do |prime|
      line << sprintf("  %#{@max_num_length}d", row * prime)
    end
    line
  end

  
end