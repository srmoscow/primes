
require File.dirname(__FILE__) + '/../lib/prime_table'

describe PrimeTable do 

  describe 'private method get_primes' do
    it 'should return an array of 6 primes starting at 1' do
      pt = PrimeTable.new(1, 6)
      pt.send(:get_primes).should == [2, 3, 5, 7, 11, 13]
    end

    it 'should return an array af 4 primes starting at 5' do
      pt = PrimeTable.new(5, 4)
      pt.send(:get_primes).should == [11, 13, 17, 19]
    end
  end

  describe 'private method generate table' do

    it 'should return an array of primes and their products' do
      # given an array of primes [11, 13, 17, 19]
      pt = PrimeTable.new(5, 4)
      pt.send(:generate_table).should == [
              "      11   13   17   19",
              " 11  121  143  187  209",
              " 13  143  169  221  247",
              " 17  187  221  289  323",
              " 19  209  247  323  361"
          ]
    end


  end

end